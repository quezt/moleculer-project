import { ServiceSchema } from 'moleculer';
import moleculerWeb from 'moleculer-web';

const serviceSchema: ServiceSchema = {
    name: 'routes',
    version: '1.0.0',
    mixins: [moleculerWeb],
    settings: {
      $noVersionPrefix: false,
      logResponseData: 'debug',
      routes: [
        {
            path: '/node/health',
            aliases: {
              'GET /': '$node.health',
            },
            mappingPolicy: 'restrict',
        },
      ],
    }
}

export = serviceSchema;