import { names } from '@actions/names.action';
import mongodbMixin from '@mixins/mongodb.mixin';
{{#if_eq logger "Bunyan"}}
import { mkdirpSync, pathExistsSync } from 'fs-extra';
{{/if_eq}}
import { ServiceBroker } from 'moleculer';
import { Cursor } from 'mongodb';

const SERVICE: string = 'mock-service';
const ACTION_NAME: string = 'names';

{{#if_eq logger "Bunyan"}}
const LOG_DIR = './target/log';
if(!pathExistsSync(LOG_DIR)) {
  mkdirpSync(LOG_DIR);
}
{{/if_eq}}
describe(`Test ${ACTION_NAME} action`, () => {

  let broker:ServiceBroker;
  const brokerOptions = { {{#if_eq logger "Console"}}
    logger: false,{{/if_eq}}{{#if_eq logger "Bunyan"}}
    logger: {
      type: 'Bunyan',
      options: {
        bunyan: {
          name: 'moleculer',
          streams: [{
            path: `${LOG_DIR}/${ACTION_NAME}.json`
          }]
        },
      },
    },{{/if_eq}}
  };
  
  beforeAll(async () => {
    broker  = new ServiceBroker(brokerOptions);
    broker.createService({
      name: SERVICE,
      version: '1.0.0',
      settings: {
        $noVersionPrefix: true,
      },
      mixins: [mongodbMixin('documents')],
      actions: {
        names
      }
    });
    await broker.start();
  });

  afterAll(async () => {
    await broker.stop();
  });

  it('should list all document names', async () => {
    const actual: Cursor = await broker.call(`${SERVICE}.names`);
    expect(actual).toBeInstanceOf(Cursor);
    const keys = Object.keys(await actual.next());
    expect(keys.length).toBe(1);
    expect(keys[0]).toBe('name');
  });
});